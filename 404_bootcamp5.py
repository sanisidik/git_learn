number = input('Please input a number : ')

if number.isnumeric() == False:
    print('please enter number')
else:
    if int(number) % 2 == 0:
        print(number, 'the number is even')
    else:
        print('{0} the number is odd'.format(number))